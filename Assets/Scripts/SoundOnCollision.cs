﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundOnCollision : MonoBehaviour {

	AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	void OnCollisionEnter2D(Collision2D other){
		audio.Play ();
	}
}
