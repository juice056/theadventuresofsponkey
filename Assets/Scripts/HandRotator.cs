﻿using UnityEngine;
using System.Collections;

public class HandRotator : MonoBehaviour {

	public Transform Sponkey;
	public Transform Hand;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 dir = Sponkey.position - Hand.position;
		//float angle = Vector2.Angle (dir, Vector2.right);
		//angle -= 90;
		//transform.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);
		Vector3 forward = new Vector3(0,dir.x, dir.y);
		//
		float angle = Mathf.Atan2(dir.y, dir.x);

		transform.rotation = Quaternion.Euler(0f, 0f, (Mathf.Rad2Deg * angle) + 90);
		//transform.rotation = Quaternion.LookRotation(forward, Vector3.up);
	}
}
