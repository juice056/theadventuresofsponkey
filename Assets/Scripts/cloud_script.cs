﻿using UnityEngine;
using System.Collections;

public class cloud_script : MonoBehaviour {

	public float speed = 0.1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 offset = new Vector2 (speed * 0.1f, 0);

		// now render the clouds with the new offset
		transform.Translate(offset, 0);
	}
}
