﻿using UnityEngine;
using System.Collections;


public class Attach : MonoBehaviour {
	Rigidbody2D monkey_rb;
	DistanceJoint2D dist_j;
	bool connected = false;



	void OnTriggerEnter2D (Collider2D other)
	{
		Debug.Log (other.tag);
		if (!connected && other.CompareTag ("Player")) {
			DistanceJoint2D dist_j = gameObject.AddComponent < DistanceJoint2D>();
			dist_j.connectedBody = other.attachedRigidbody;
			dist_j.distance = 0;
			dist_j.breakForce = 100;
			connected = true;
		}
	}
}
