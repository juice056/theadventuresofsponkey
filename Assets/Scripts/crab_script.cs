﻿using UnityEngine;
using System.Collections;
using System;

public class crab_script : MonoBehaviour {

		public float Speed = 0f;
		private float movex = 1f;
		bool goto1 = false;
		public Transform point1, point2;
		Rigidbody2D rb2D;


		// Use this for initialization
		void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
		}

		// Update is called once per frame
		void FixedUpdate () {

		/*crab_t = GetComponentInParent<Transform>();
		point1 = crab_t.transform.Find ("point1");//GetChild<Transform>(0);
		point2 = crab_t.transform.Find ("point2");//GetChild<Transform>(1);
		rb2D = GetComponent<Rigidbody2D>();*/

	
		if (goto1) {
			if (Mathf.Abs (transform.position.x - point1.position.x) < 1 ) {
				goto1 = !goto1;
				movex = -movex;
			} 
		} else if (Mathf.Abs (transform.position.x - point2.position.x) < 1) {
			goto1 = !goto1;
			movex = -movex;
		} 

		//move

		rb2D.velocity = new Vector2 (movex * Speed, rb2D.velocity.y);
	}
		
}
