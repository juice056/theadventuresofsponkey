﻿using UnityEngine;
using System.Collections;


public class InputController : MonoBehaviour {

	public string horizontalInputAxis;
	public string verticalInputAxis;
	public string inputButton;

	public delegate void ButtonUpEvent();
	public ButtonUpEvent OnButtonUp;
	public delegate void ButtonDownEvent();
	public ButtonUpEvent OnButtonDown;
	public delegate void ButtonHeldEvent();
	public ButtonUpEvent OnButtonHeld;

	[HideInInspector]
	public bool buttonIsPressed;

	[HideInInspector]
	public Vector2 axisValues;

	bool buttonDown;
	bool buttonUp;

	// Use this for initialization
	void Start () {
		buttonIsPressed = false;
		buttonDown = false;
		buttonUp = false;
	}
	
	// Update is called once per frame
	void Update () {
		float horizontalAxisValue = Input.GetAxis (horizontalInputAxis); //-1,1
		float verticalAxisValue = Input.GetAxis (verticalInputAxis);
		axisValues = new Vector2 (horizontalAxisValue, verticalAxisValue);
		if (Input.GetButtonDown(inputButton)) {
			buttonIsPressed = true;
			buttonDown = true;
			OnButtonDown ();
		}
		else if(Input.GetButtonUp(inputButton)){
			buttonUp = true;
			buttonIsPressed = false;
			OnButtonUp ();
		}

		
	}

	void FixedUpdate(){
		if (buttonUp) {
			OnButtonUp ();
			buttonUp = false;
		}
		if (buttonDown) {
			OnButtonDown ();
			buttonDown = false;
		}
		if (buttonIsPressed && !buttonDown)
			OnButtonHeld ();
	}

}
