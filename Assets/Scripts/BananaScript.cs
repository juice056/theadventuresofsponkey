﻿using UnityEngine;
using System.Collections;

public class BananaScript : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D rigidBody;
	bool hasFallen = false;
	void Start () {
		rigidBody = GetComponent<Rigidbody2D> ();
	}
	

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log (other.tag);
		if (!hasFallen && other.CompareTag("Stick")) {
			rigidBody.isKinematic = false;
			hasFallen = true;
			
		}
	}
}
