﻿using UnityEngine;
using System.Collections;

public class SpriteChanger : MonoBehaviour {

	public Sprite open;
	public Sprite closed;
	InputController controller;
	SpriteRenderer spriteRenderer;
	// Use this for initialization
	void Start () {
		controller = GetComponentInParent<InputController> ();
		controller.OnButtonUp += Open ;
		controller.OnButtonDown += Close ;

		spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Open () {
		spriteRenderer.sprite = open;
	}

	void Close() {
		spriteRenderer.sprite = closed;
	}


}
