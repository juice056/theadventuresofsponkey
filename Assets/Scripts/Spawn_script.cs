﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawn_script : MonoBehaviour {
	
	public GameObject[] cloud;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		int dice = Random.Range(1,1001);
		if (dice < 5) {
			Instantiate (cloud [Random.Range (0, cloud.Length)], transform.position + Random.insideUnitSphere * 10 , Quaternion.identity);
		}
	}
}
