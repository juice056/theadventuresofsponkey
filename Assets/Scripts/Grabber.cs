﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Grabber : MonoBehaviour {
	InputController controller;
	Rigidbody2D rigidBody;

	public Rigidbody2D sponkey;
	public float maxVelocity;
	public float maxForce;

	bool isFixed;
	bool isGrabbing;

	public GameObject grabbedObject;
	List<Collider2D> collidedObjects;

	void Start()
	{
		rigidBody = transform.GetComponentInParent<Rigidbody2D> ();
		isFixed = false;
		isGrabbing = false;
		controller = transform.GetComponentInParent<InputController> ();
		controller.OnButtonUp += ButtonReleased;
		controller.OnButtonDown += ButtonPressed;
		controller.OnButtonHeld += ButtonHeld;
		collidedObjects = new List<Collider2D>();
	}

	// Update is called once per frame
	void FixedUpdate () {
		Vector2 axisValue = controller.axisValues;

		if(!isFixed)
			rigidBody.velocity = maxVelocity * axisValue;

	}
	void OnTriggerEnter2D(Collider2D other){
		collidedObjects.Add (other);
	}
	void OnTriggerExit2D(Collider2D other){

		List<Collider2D> allInstances = new List<Collider2D> ();
		allInstances = collidedObjects.FindAll (t => t.name == other.name);
		foreach (var instance in allInstances)
			collidedObjects.Remove (instance);

		if (grabbedObject == other.gameObject) {
			ButtonReleased ();
		}
	}



	void ButtonReleased()	{
		rigidBody.isKinematic = false;
		isFixed = false;
		isGrabbing = false;
		if (grabbedObject && grabbedObject.layer == LayerMask.NameToLayer ("InteractableObjects")) {
			Rigidbody2D grabbedObjectRigidBody = grabbedObject.GetComponent<Rigidbody2D> ();
			grabbedObjectRigidBody.gravityScale = 1;
			grabbedObjectRigidBody.freezeRotation = false;
		}
		grabbedObject = null;
	}


	void ButtonPressed()	{

		collidedObjects.Sort (SortingLayerComparer);

		if (collidedObjects.Count > 0 && !isGrabbing) {
			grabbedObject = collidedObjects [0].gameObject;
			isGrabbing = true;
		}

	}

	int SortingLayerComparer(Collider2D one, Collider2D two){
		SpriteRenderer leftRenderer = one.gameObject.GetComponent<SpriteRenderer> ();
		SpriteRenderer rightRenderer = two.gameObject.GetComponent<SpriteRenderer> ();
		if (!leftRenderer && !rightRenderer)
			return 0;
		if (!leftRenderer)
			return 1;
		if (!rightRenderer)
			return -1;
		
		int leftId = leftRenderer.sortingLayerID;
		int rightId = rightRenderer.sortingLayerID;

		if (leftId < rightId)
			return -1;
		else if (leftId == rightId)
			return 0;
		else 
			return 1;
	}

	IEnumerator LevelComplete(){
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
	}
	void ButtonHeld(){
		if (grabbedObject) {
			if (grabbedObject.CompareTag ("Banana")) {
				StartCoroutine (LevelComplete ());
			}
			if (grabbedObject.layer == LayerMask.NameToLayer ("InteractableObjects")) {

				Rigidbody2D otherRigidBody = grabbedObject.GetComponent<Rigidbody2D> ();
				otherRigidBody.velocity = rigidBody.velocity;
				otherRigidBody.isKinematic = false;
				otherRigidBody.gravityScale = 0;
				otherRigidBody.freezeRotation = true;

			}

			if (grabbedObject.layer == LayerMask.NameToLayer ("StaticObjects")) {
				isFixed = true;
				sponkey.AddForce (-controller.axisValues * maxForce);
				rigidBody.isKinematic = true;
			}
		}
	}
}
